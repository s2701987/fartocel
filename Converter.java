package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Converter {

    double getTemperature(String temperature) {

        return (Integer.parseInt(temperature)*1.8) + 32;

    }
}
